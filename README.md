# Encryptor-test-method-in-Java
Encryptor test method in Java



import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class EncryptorTest {
  
  public static void main(String[] args)
  {
    try
     { 
     JFileChooser chooser = new JFileChooser();
     if (chooser.showOpenDialog(null)
      != JFileChooser.APPROVE_OPTION)
      System.exit(0);
      
      File outFile = chooser.getSelectedFile();
      String input = JOptionPane.showInputDialog("Key");
      int key = Integer.parseInt(input);
      Encryptor crypt = new Encryptor(key);
      crypt.encryptFile(inFile, outFile);
    }
    catch (NumberFormatException exception)
    {
       System.out.println("Key must be in an integer: " + exception);
    }  
    catch (IOException exception)
    {
       System.out.println("Error processing file: " + exception);
    }
    System.out.println(0);
  }
 } 
      

